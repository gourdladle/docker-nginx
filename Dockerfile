FROM nginx:1.17.8-alpine

MAINTAINER Will <shaoweizheng@163.com|qq252075062>

COPY gosu-amd64 /usr/local/bin/gosu
COPY gosu-amd64.asc /usr/local/bin/gosu.asc
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

WORKDIR /wwwroot

ENV GOSU_VERSION 1.11

RUN set -eux; \
    mv /etc/apk/repositories /etc/apk/repositories.default; \
    echo -e "http://mirrors.aliyun.com/alpine/v3.10/main\nhttp://mirrors.aliyun.com/alpine/v3.10/community" > /etc/apk/repositories; \
    apk update; \
    apk add --no-cache --virtual .gosu-deps \
        ca-certificates \
        dpkg \
        gnupg; \
    dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
#    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
#    wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
    export GNUPGHOME="$(mktemp -d)"; \
#    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
    gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
    gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
    command -v gpgconf && gpgconf --kill all || :; \
    rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
    apk del --no-network .gosu-deps; \
    chmod +x /usr/local/bin/gosu; \
    gosu --version; \
    gosu nobody true; \
    rm -rf /var/cache/apk/*; \
    rm -rf /var/lib/apk/*; \
    rm -rf /etc/apk/cache/*; \
    chmod a+x /usr/local/bin/entrypoint.sh;
    
ENTRYPOINT ["entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
