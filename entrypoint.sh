#!/bin/sh
if [ $NGINX_USER -a -n $NGINX_USER -a $NGINX_USER_ID -a -n $NGINX_USER_ID ]; then
    id $NGINX_USER_ID >& /dev/null
    if [ $? -ne 0 ]; then
        adduser $NGINX_USER -u $NGINX_USER_ID;
    fi
fi

